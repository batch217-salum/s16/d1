console.log("Hello World!");

// [SECTION] Arithmetic Operation
// + , - , * , /

let x = 1397;
let y = 7831;

let sum = x + y;
console.log(" Result of addition operator: " + sum);

let difference = x - y;
console.log(" Result of subtraction operator: " + difference);

let product = x * y;
console.log(" Result of multiplication operator: " + product);

let quotient = x / y;
console.log(" Result of division operator: " + quotient);

let remainder = y % x;
console.log(" Result of modulo operator: " + remainder);

// [SECTION] Assignment Operator
// Basic Assignment operator is (=) equal sign.

let assignmentNumber = 8;

// addition assignment

assignmentNumber = assignmentNumber + 2;
console.log(" Result of addition assignmen operator: " + assignmentNumber);

// shorthand for assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(" Result of addition assignmen operator: " + assignmentNumber);

// (-=, *= , /=)
assignmentNumber -= 2;
console.log(" Result of subtraction operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log(" Result of multiplication operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log(" Result of division operator: " + assignmentNumber);

// Multiple Operators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5
console.log(" Result of mdas: " + mdas);

let pemdas = 1 + (2-3) * (4 / 5);
console.log(" Result of pemdas: " + pemdas);

// incrementation and decrementation

let z = 1;

let increment = ++z;
console.log("Result of pre-increment: "+ increment);
console.log("Result of pre-increment: "+ z);

increment = z++;
console.log("Result of post-increment: "+ increment);
console.log("Result of post-increment: "+ z);

let decrement = --z;
console.log("Result of post-increment: "+ decrement);
console.log("Result of post-increment: "+ z);

decrement = z--;
console.log("Result of post-increment: "+ decrement);
console.log("Result of post-increment: "+ z);

// [SECTION] Type Coercion

let numA = '10' ;
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion)


let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion)

let numE = true + 1;
console.log(numE);

let numF = false + 1;
console.log(numF);

// Comparison Operator

let juan = "juan";

// equality operator (==)
// = assignment of value
//  == comparision equality

console.log	(1 == 1);
console.log	(1 == 2);
console.log	(1 == '1');
console.log	(0 == false);
console.log	("juan" == "juan");
console.log	(juan == "juan");

//IneQuality Operator (!=)
//  ! = not

console.log	(1 != 1);
console.log	(1 != 2);


// Strictly Equlity Operator (===)
console.log	(1 === '1');
console.log	("juan" === juan);
console.log	(0 === false);

// Strictly IneQuality Operator (!==)
console.log	(1 !== '1');
console.log	("juan" !== juan);
console.log	(0 !== false);


// [SECTION] Relational Operator
// > , < , =

let a = 50;
let b = 60;

let isGreaterThan = a > b;
console.log	(isGreaterThan);

let isLessThan = a < b;
console.log	(isLessThan);

let isGTORequal = a >= b;
console.log	(isGTORequal);

let isLTORequal = a <= b;
console.log	(isLTORequal);


// [SECTION] Logical Operator
// && , || , !

let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Logical && Result: "+ allRequirementsMet);

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Logical OR Result: "+ someRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Logical NOT Result: "+ someRequirementsNotMet);